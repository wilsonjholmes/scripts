#!/bin/sh -e

cat $1 | grep -E ":" -v | awk '!seen[$0]++' | tr '\n' ' '
