#!/bin/bash -e

#TODO:

#| jpg | raw |
#|--- |--- |
#| thing1 | thing1 |
#| thing 2 |   |
#|   | thing 3 |

# Display Help
Help()
{
	echo "Delete the jpg counterpart of raw photos and visa-versa."
	echo
	echo "Syntax: syncphotodel [-h] DIRECTORY"
	echo "options:"
	echo "h     Print this Help."
	echo
}

# Get the options
while getopts ":h" option; do
	case $option in
		h) # display Help
			Help
			exit;;
		\?) # Invalid option
			echo "Error: Invalid option"
			exit;;
	esac
done

# Check and see if path is valid
#if ! [[ -d "$1" && -d "$2"]]
#then
#    echo "One (or both) of the directory paths are incorrect, or are non-existent."
#	exit
#fi

for JPG_FILE in ./em1/jpg/*; do
    JPGname=`basename $JPG_FILE .JPG`
    echo $JPGname >> jpg-list.txt
done

for RAW_FILE in ./em1/raw/*; do
    RAWname=`basename $RAW_FILE .ORF`
    echo $RAWname >> raw-list.txt
done

diff jpg-list.txt raw-list.txt | less

rm jpg-list.txt raw-list.txt
# Delete the jpg files that do not have a raw counterpart
#for file in $1/*
#do
#filestr=`basename $file`
#if [ `ls $1 | grep -c $filestr` -eq 0 ]; then
#    echo "$file does not have a counterpart."
#fi
#done
