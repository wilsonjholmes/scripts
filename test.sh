#!/bin/bash -eu

for SCRIPT in ~/git/scripts/*.sh; do
    alias $(basename $SCRIPT .sh)='$SCRIPT'
done
