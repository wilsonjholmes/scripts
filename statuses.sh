#!/bin/bash -eu

Help()
{
	# Display Help
	echo "Will run git status for all repos in the current directory."
	echo
	echo "Syntax: statuses [-h] [-v]"
	echo "options:"
	echo "h     Print this Help."
    echo "v     Be more verbose."
	echo
}

Main()
{
    find . -maxdepth 1 -mindepth 1 -type d -exec sh -c '(echo {} && cd {} && git status '"$VERBOSE"')' \;
}

# required: git
if ! command -v git &>/dev/null; then
	echo
	echo [!] Command git is required. Please install git.
	exit 1
else
	echo -e git
fi

# Get the options
while getopts ":hv" option; do
	case $option in
		h) # display Help
			Help
			exit;;
        v) # verbose mode
			VERBOSE=""
            Main
			exit;;
        \?) # Invalid option
            echo "Error: Invalid option"
            exit;;
	esac
done

VERBOSE="-s"
Main