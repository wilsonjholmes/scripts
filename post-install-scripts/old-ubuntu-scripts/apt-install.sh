#!/bin/bash

# exit when any command fails
set -e

# Update system and install new programs
sudo apt update; sudo apt upgrade -y
sudo apt install -y curl wget git zsh neovim firefox tmux cmatrix neofetch \
    python3-pip openssh-server xclip screen figlet lolcat tree arp-scan \
    libssl-dev build-essential clang flatpak libfontconfig-dev libfreetype-dev

