#!/bin/bash

# exit when any command fails
set -e

# Install Zotero
wget -qO- https://github.com/retorquere/zotero-deb/releases/download/apt-get/install.sh | sudo bash
sudo apt update
sudo apt install -y zotero

# Install Cura-LE
sh -c "$(curl -fsSL https://gitlab.com/wilsonjholmes/cura-le-installation-helper/-/raw/main/installCuraLE.sh)"
sudo rm -r cura-lulzbot-ubuntu-stuffs

