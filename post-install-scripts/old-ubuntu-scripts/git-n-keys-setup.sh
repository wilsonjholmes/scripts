#!/bin/bash

# exit when any command fails
set -e

# Setup git things
git config --global user.name "Wilson Holmes"
git config --global user.email wilsonjholmes@gmail.com
git config --global color.ui auto

# Setup ssh-keys
ssh-keygen -t ed25519 -C "wilsonjholmes@gmail.com"
# Start the ssh-agent
eval `ssh-agent -s`
# Add the key
ssh-add
# Copy public key to clipboard
xclip -sel clip < ~/.ssh/id_ed25519.pub

# Wait for user to paste key into github or gitlab
echo
read -n 1 -s -r -p "Your ssh key has been copied to your clipboard. Paste it to your remote of choice and then press any key to continue: "
echo

