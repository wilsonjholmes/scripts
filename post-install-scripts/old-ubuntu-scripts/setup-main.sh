#!/bin/bash

# exit when any command fails
set -e

# Check to see which distro I am running
DISTRO=`uname -n`

# Install cargo programs
./cargo-install.sh

# Install package manager sepecific programs
if [[ -f /usr/bin/apt ]]; then
    # Setup and install with apt
    ./apt-setup.sh
    ./apt-install.sh
    
    # Install ROS
    ./ros-ubuntu-install.sh
    
    # Install .deb programs
    ./deb-install.sh

    # Compile needed programs from source
    ./debian-based-builds.sh

elif [[ -f /usr/bin/pacman ]]; then
    # Setup and install with pacman
    ./pacman-setup.sh
    ./pacman-install.sh

    # Compile needed programs from source
    ./arch-based-builds-yay.sh

elif [[ -f /usr/bin/dnf ]]; then
    # Setup and install with pacman
    ./dnf-setup.sh
    ./dnf-install.sh

    # Compile needed programs from source
    ./rhel-based-builds.sh
fi

# Install flatpaks
./flatpak-install.sh

# Install appimages
./appimage-install.sh

# Setup git and ssh-key things
./git-n-keys-setup.sh

# Clones git repos
./clone-repos.sh

# Setup oh-my-zsh
./setup-oh-my-zsh.sh

# Setup dotfiles
./dotfiles.sh

# Fun ending message
figlet "... and we're back to
work!" | lolcat
