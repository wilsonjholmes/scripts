#!/bin/bash

# exit when any command fails
set -e

cd ~

# Setup dotfiles bare repo thingy
git clone --separate-git-dir=$HOME/.dotfiles git@gitlab.com:wilsonjholmes/dotfiles.git dotfiles-tmp
rsync --recursive --verbose --exclude '.git' dotfiles-tmp/ $HOME/
rm --recursive dotfiles-tmp
git --git-dir=$HOME/.dotfiles/ --work-tree=$HOME config status.showUntrackedFiles no
git --git-dir=$HOME/.dotfiles/ --work-tree=$HOME remote set-url origin git@gitlab.com:wilsonjholmes/dotfiles.git

