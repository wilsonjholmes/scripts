#!/bin/bash

# exit when any command fails
set -e

# Build and install neovide
yay -S --noconfirm neovide
