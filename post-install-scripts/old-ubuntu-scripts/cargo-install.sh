#!/bin/bash

# exit when any command fails
set -e

# Install rust
curl --proto '=https' --tlsv1.2 -LsSf https://sh.rustup.rs | sh -s -- -y

# Use cargo to install rust programs
~/.cargo/bin/cargo install exa tealdeer du-dust ripgrep bat fd-find \
    cargo-update

