#!/bin/bash

# exit when any command fails
set -e

# Check if xenial, bionic, or focal
. /etc/os-release
if [ $VERSION_ID == 16.04 ]; then
	# Setup your computer to accept software from packages.ros.org
	sudo sh -c 'echo "deb http://packages.ros.org/ros/ubuntu $(lsb_release -sc) main" > /etc/apt/sources.list.d/ros-latest.list'

	# Set up your keys
	curl -s https://raw.githubusercontent.com/ros/rosdistro/master/ros.asc | sudo apt-key add -
	
	# Install ROS
	sudo apt-get update
	sudo apt-get install -y ros-kinetic-desktop-full
	sudo apt install -y python-rosdep python-rosinstall python-rosinstall-generator python-wstool build-essential

	# Initialize rosdep
	sudo rosdep init
	rosdep update

elif [ $VERSION_ID == 18.04 ]; then
	# Setup your computer to accept software from packages.ros.org
	sudo sh -c 'echo "deb http://packages.ros.org/ros/ubuntu $(lsb_release -sc) main" > /etc/apt/sources.list.d/ros-latest.list'

	# Set up your keys
	curl -s https://raw.githubusercontent.com/ros/rosdistro/master/ros.asc | sudo apt-key add -
	
	# Install ROS
	sudo apt update
	sudo apt install -y ros-melodic-desktop-full
	sudo apt install -y python-rosdep python-rosinstall python-rosinstall-generator python-wstool build-essential

	# Initialize rosdep
	sudo rosdep init
	rosdep update

elif [ $VERSION_ID == 20.04 ]; then
	# Setup your computer to accept software from packages.ros.org
	sudo sh -c 'echo "deb http://packages.ros.org/ros/ubuntu $(lsb_release -sc) main" > /etc/apt/sources.list.d/ros-latest.list'

	# Set up your keys
	curl -s https://raw.githubusercontent.com/ros/rosdistro/master/ros.asc | sudo apt-key add -
	
	# Install ROS
	sudo apt update
	sudo apt install -y ros-noetic-desktop-full
	sudo apt install -y python3-rosdep python3-rosinstall python3-rosinstall-generator python3-wstool build-essential

	# Initialize rosdep
	sudo rosdep init
	rosdep update
fi

