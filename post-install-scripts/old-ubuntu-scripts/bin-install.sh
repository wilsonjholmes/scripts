#!/bin/bash

# exit when any command fails
set -e

# Install exercism
mkdir exercism-cli
cd exercism-cli
wget https://github.com/exercism/cli/releases/download/v3.0.13/exercism-3.0.13-linux-x86_64.tar.gz
tar -xf exercism-3.0.13-linux-x86_64.tar.gz
sudo cp exercism /bin
cd ..
rm -r exercism-cli

