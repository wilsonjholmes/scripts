#!/bin/bash

# exit when any command fails
set -e

# Enable flatpak repo
flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo

# Install flatpak apps
flatpak install flathub -y com.spotify.Client/x86_64/stable com.uploadedlobster.peek/x86_64/stable org.signal.Signal/x86_64/stable com.discordapp.Discord/x86_64/stable app/com.obsproject.Studio/x86_64/stable
