#!/bin/bash

# exit when any command fails
set -e

# Clone ~/src repos
git clone https://github.com/openscad/openscad.git ~/src/openscad
git clone https://github.com/Mayccoll/Gogh.git ~/src/gogh

# Clone ~/git repos
git clone git@gitlab.com:wilsonjholmes/learn-ros.git ~/git/learn-ros
git clone git@gitlab.com:wilsonjholmes/notes.git ~/git/notes
git clone git@gitlab.com:wilsonjholmes/scripts.git ~/git/scripts
git clone git@gitlab.com:wilsonjholmes/school-notes.git ~/git/school-notes
git clone git@gitlab.com:wilsonjholmes/treasure-box.git ~/git/treasure-box
git clone git@gitlab.com:wilsonjholmes/wallpapers.git ~/git/wallpapers
git clone git@github.com:wilsonjholmes/InfiniTime.git ~/git/infinitime
#git clone git@gitlab.com:mtu-most/most_arduinophcontrol.git ~/git/most_arduinophcontrol
git clone git@gitlab.com:mtu-most/most_chibio.git ~/git/most_chibio
#git clone git@gitlab.com:mtu-most/most_i2ctesting.git ~/git/most_i2ctesting
#git clone git@gitlab.com:mtu-most/most_lightning.git ~/git/most_lightning

