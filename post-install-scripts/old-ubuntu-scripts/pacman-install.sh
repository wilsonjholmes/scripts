#!/bin/bash

# exit when any command fails
set -e

sudo pacman -S --noconfirm curl wget git zsh neovim firefox tmux cmatrix \
    neofetch python-pip openssh xclip screen figlet lolcat tree arp-scan \
    base-devel clang flatpak yay
