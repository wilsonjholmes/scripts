#!/bin/bash

# exit when any command fails
set -e

# Clone source into src
git clone https://github.com/neovide/neovide ~/src/neovide

# Build and install neovide
cd ~/src/neovide && ~/.cargo/bin/cargo build --release
sudo cp ./target/release/neovide /bin
