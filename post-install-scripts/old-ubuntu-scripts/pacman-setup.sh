#!/bin/bash

# exit when any command fails
set -e

# Get fastest pacman mirrors
sudo pacman-mirrors --geoip; sudo pacman -Syyu --noconfirm
