#!/bin/sh

set -e
# Update system and install new programs
sudo apt update; sudo apt upgrade -y
sudo apt install -y curl wget git zsh htop neovim firefox tmux cmatrix neofetch \
    python3-pip openssh-server xclip screen figlet lolcat tree arp-scan build-essential clang


    cd ~

    # Setup dotfiles bare repo thingy
    git clone --separate-git-dir=$HOME/.dotfiles git@gitlab.com:wilsonjholmes/dotfiles.git dotfiles-tmp
    rsync --recursive --verbose --exclude '.git' dotfiles-tmp/ $HOME/
    rm --recursive dotfiles-tmp
    git --git-dir=$HOME/.dotfiles/ --work-tree=$HOME config status.showUntrackedFiles no
    git --git-dir=$HOME/.dotfiles/ --work-tree=$HOME remote set-url origin git@gitlab.com:wilsonjholmes/dotfiles.git

    # Fun ending message
    figlet "... and we're back to
    work!" | lolcat
