#!/bin/sh

set -e

function pip-install() {
    
    # Install pywal
    #sudo pip3 install pywal
    :

}

function appimage-install() {

    # TODO Add appimage stuff here
    mkdir appimages; cd appimages
    curl -LO https://github.com/neovim/neovim/releases/latest/download/nvim.appimage
    chmod u+x nvim.appimage

}

function apt-install() {

    # Update system and install new programs
    sudo apt update; sudo apt upgrade -y
    sudo apt install -y curl wget git zsh htop neovim firefox tmux cmatrix neofetch \
    python3-pip openssh-server xclip screen figlet lolcat tree arp-scan \
    libssl-dev build-essential clang flatpak libfontconfig-dev libfreetype-dev \
    kitty

}

function apt-setup() {

    # TODO: Setup MTU mirrors here
    :

}

function arch-based-builds-yay() {

    # Build and install neovide
    yay -S --noconfirm neovide

}

function bin-install() {

    # Install exercism
    mkdir exercism-cli
    cd exercism-cli
    wget https://github.com/exercism/cli/releases/download/v3.0.13/exercism-3.0.13-linux-x86_64.tar.gz
    tar -xf exercism-3.0.13-linux-x86_64.tar.gz
    sudo cp exercism /bin
    cd ..
    rm -r exercism-cli

}

function cargo-install() {

    # Install rust
    curl --proto '=https' --tlsv1.2 -LsSf https://sh.rustup.rs | sh -s -- -y

    # Use cargo to install rust programs
    ~/.cargo/bin/cargo install exa tealdeer du-dust ripgrep bat fd-find \
    cargo-update procs ytop sd grex

}

function clone-repos() {

    # Clone ~/src repos
    git clone https://github.com/openscad/openscad.git ~/src/openscad
    git clone https://github.com/Mayccoll/Gogh.git ~/src/gogh

    # Clone ~/git repos
    git clone git@gitlab.com:wilsonjholmes/learn-ros.git ~/git/learn-ros
    git clone git@gitlab.com:wilsonjholmes/notes.git ~/git/notes
    git clone git@gitlab.com:wilsonjholmes/scripts.git ~/git/scripts
    git clone git@gitlab.com:wilsonjholmes/school-notes.git ~/git/school-notes
    git clone git@gitlab.com:wilsonjholmes/treasure-box.git ~/git/treasure-box
    git clone git@gitlab.com:wilsonjholmes/wallpapers.git ~/git/wallpapers
    git clone git@gitlab.com:wilsonjholmes/wilsonjholmes.gitlab.io.git ~/git/website
    git clone git@github.com:wilsonjholmes/InfiniTime.git ~/git/infinitime
    
    #git clone git@gitlab.com:mtu-most/most_arduinophcontrol.git ~/git/most_arduinophcontrol
    git clone git@gitlab.com:mtu-most/most_chibio.git ~/git/most_chibio
    #git clone git@gitlab.com:mtu-most/most_i2ctesting.git ~/git/most_i2ctesting
    #git clone git@gitlab.com:mtu-most/most_lightning.git ~/git/most_lightning

}

function debian-based-builds() {
    # Clone source into src
    git clone https://github.com/neovide/neovide ~/src/neovide

    # Build and install neovide
    cd ~/src/neovide && ~/.cargo/bin/cargo build --release
    sudo cp ./target/release/neovide /bin

}

function deb-install() {

    # Install Zotero wget -qO- https://github.com/retorquere/zotero-deb/releases/download/apt-get/install.sh | sudo bash sudo apt update
    sudo apt install -y zotero

    # Install Cura-LE
    sh -c "$(curl -fsSL https://gitlab.com/wilsonjholmes/cura-le-installation-helper/-/raw/main/installCuraLE.sh)"
    sudo rm -r cura-lulzbot-ubuntu-stuffs

}

function dnf-install() {

    # Update system and install new programs
    sudo dnf update -y
    sudo dnf install -y curl wget git zsh neovim firefox tmux cmatrix neofetch \
        python3-pip openssh-server xclip screen figlet lolcat tree arp-scan \
        gcc clang flatpak util-linux-user kitty
    sudo dnf groupinstall -y "Development Tools" "Development Libraries"

}

function dnf-setup() {

    # TODO Setup MTU mirrors here
    :

}

function dotfiles() {

    cd ~

    # Setup dotfiles bare repo thingy
    git clone --separate-git-dir=$HOME/.dotfiles git@gitlab.com:wilsonjholmes/dotfiles.git dotfiles-tmp
    rsync --recursive --verbose --exclude '.git' dotfiles-tmp/ $HOME/
    rm --recursive dotfiles-tmp
    git --git-dir=$HOME/.dotfiles/ --work-tree=$HOME config status.showUntrackedFiles no
    git --git-dir=$HOME/.dotfiles/ --work-tree=$HOME remote set-url origin git@gitlab.com:wilsonjholmes/dotfiles.git

}

function flatpak-install() {

    # Enable flatpak repo
    flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo

    # Install flatpak apps
    flatpak install flathub -y com.spotify.Client/x86_64/stable \
        com.uploadedlobster.peek/x86_64/stable \
        org.signal.Signal/x86_64/stable \
        com.discordapp.Discord/x86_64/stable \
        app/com.obsproject.Studio/x86_64/stable

}

function git-n-keys-setup() {

    # Setup git things
    git config --global user.name "Wilson Holmes"
    git config --global user.email wilsonjholmes@gmail.com
    git config --global color.ui auto

    # Setup ssh-keys
    ssh-keygen -t ed25519 -C "wilsonjholmes@gmail.com"
    # Start the ssh-agent
    eval `ssh-agent -s`
    # Add the key
    ssh-add
    # Copy public key to clipboard
    xclip -sel clip < ~/.ssh/id_ed25519.pub

    # Wait for user to paste key into github or gitlab
    echo
    read -n 1 -s -r -p "Your ssh key has been copied to your clipboard. Paste it to your remote of choice and then press any key to continue: "
    echo

}

function pacman-install() {

    sudo pacman -S --noconfirm curl wget git zsh neovim firefox tmux cmatrix \
        neofetch python-pip openssh xclip screen figlet lolcat tree arp-scan \
        base-devel clang flatpak yay kitty rofi

}

function pacman-setup() {

    # Get fastest pacman mirrors
    sudo pacman-mirrors --geoip; sudo pacman -Syyu --noconfirm

}

function rhel-based-builds() {

    # Clone source into src
    git clone https://github.com/neovide/neovide ~/src/neovide

    # Build and install neovide
    cd ~/src/neovide && ~/.cargo/bin/cargo build --release
    sudo cp ./target/release/neovide /bin

}

function ros-ubuntu-install() {

    # Check if xenial, bionic, or focal
    . /etc/os-release
    if [ $VERSION_ID == 16.04 ]; then
	    # Setup your computer to accept software from packages.ros.org
	    sudo sh -c 'echo "deb http://packages.ros.org/ros/ubuntu $(lsb_release -sc) main" > /etc/apt/sources.list.d/ros-latest.list'

	    # Set up your keys
	    curl -s https://raw.githubusercontent.com/ros/rosdistro/master/ros.asc | sudo apt-key add -
	
	    # Install ROS
	    sudo apt-get update
	    sudo apt-get install -y ros-kinetic-desktop-full
	    sudo apt install -y python-rosdep python-rosinstall python-rosinstall-generator python-wstool build-essential

	    # Initialize rosdep
	    sudo rosdep init
	    rosdep update

    elif [ $VERSION_ID == 18.04 ]; then
        # Setup your computer to accept software from packages.ros.org
        sudo sh -c 'echo "deb http://packages.ros.org/ros/ubuntu $(lsb_release -sc) main" > /etc/apt/sources.list.d/ros-latest.list'

        # Set up your keys
        curl -s https://raw.githubusercontent.com/ros/rosdistro/master/ros.asc \
            | sudo apt-key add -
        
        # Install ROS
        sudo apt update
        sudo apt install -y ros-melodic-desktop-full
        sudo apt install -y python-rosdep python-rosinstall \
            python-rosinstall-generator python-wstool build-essential

        # Initialize rosdep
        sudo rosdep init
        rosdep update

    elif [ $VERSION_ID == 20.04 ]; then
        # Setup your computer to accept software from packages.ros.org
        sudo sh -c 'echo "deb http://packages.ros.org/ros/ubuntu $(lsb_release -sc) main" > /etc/apt/sources.list.d/ros-latest.list'

        # Set up your keys
        curl -s https://raw.githubusercontent.com/ros/rosdistro/master/ros.asc \
            | sudo apt-key add -
        
        # Install ROS
        sudo apt update
        sudo apt install -y ros-noetic-desktop-full
        sudo apt install -y python3-rosdep python3-rosinstall \
            python3-rosinstall-generator python3-wstool build-essential

        # Initialize rosdep
        sudo rosdep init
        rosdep update
    fi

}

function setup-oh-my-zsh() {

# Install oh-my-zsh
CHSH=no RUNZSH=no sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"

# Get oh-my-zsh plugins
git clone https://github.com/zsh-users/zsh-syntax-highlighting.git \
    ~/.oh-my-zsh/custom/plugins/zsh-syntax-highlighting
git clone https://github.com/zsh-users/zsh-autosuggestions.git \
    ~/.oh-my-zsh/custom/plugins/zsh-autosuggestions
git clone https://github.com/zsh-users/zsh-history-substring-search.git \
    ~/.oh-my-zsh/custom/plugins/zsh-history-substring-search
git clone https://github.com/zsh-users/zsh-completions.git \
    ~/.oh-my-zsh/custom/plugins/zsh-completions

# Set zsh as default shell
chsh -s /bin/zsh $USER

}

function setup-main() {

    # Check to see which distro I am running
    DISTRO=`uname -n`

    # Install package manager sepecific programs
    if [[ -f /usr/bin/apt ]]; then
        # Setup and install with apt
        apt-setup
        apt-install
        
        # Install ROS
        ros-ubuntu-install
        
        # Install .deb programs
        deb-install

        # Compile needed programs from source
        debian-based-builds

    elif [[ -f /usr/bin/pacman ]]; then
        # Setup and install with pacman
        pacman-setup
        pacman-install

        # Compile needed programs from source
        arch-based-builds-yay

    elif [[ -f /usr/bin/dnf ]]; then
        # Setup and install with pacman
        dnf-setup
        dnf-install

        # Compile needed programs from source
        #rhel-based-builds
    fi

    # Install python modules
    pip-install

    # Install cargo programs
    cargo-install

    # Install flatpaks
    flatpak-install

    # Install appimages
    appimage-install

    # Setup oh-my-zsh
    setup-oh-my-zsh

    # Setup dotfiles
    dotfiles

    # GNOME setup
    #gsettings set org.gnome.shell favorite-apps "$(gsettings get org.gnome.shell favorite-apps | sed s/.$//), 'steam.desktop']" 
    #TODO: make an array and store favorite applications
    #TODO: enter password only once for the ssh-key and the sudo stuff
    #TODO: move the wget, curl, and git pulls to differnt function so that cargo install
    # can be done before the build thing... or just install cargo sooner...

# Setup git and ssh-key things
    git-n-keys-setup

    # Clones git repos
    clone-repos

    # Fun ending message
    figlet "... and we're back to
    work!" | lolcat

}

setup-main
