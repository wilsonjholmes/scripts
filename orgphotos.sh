#!/bin/bash -e

# Display Help
Help()
{
	echo "Easily organize photos from camera sd card."
	echo
	echo "Syntax: orgphotos [-h] DIRECTORY"
	echo "options:"
	echo "h     Print this Help."
	echo
}

# Get the options
while getopts ":h" option; do
	case $option in
		h) # display Help
			Help
			exit;;
		\?) # Invalid option
			echo "Error: Invalid option"
			exit;;
	esac
done

# Check and see if path is valid
if ! [[ -d "$1" ]]
then
	echo "Directory path is incorrect, or does not exist."
	exit
fi

# Check for .JPGs
JPG_Count=`ls -1 *.JPG 2>/dev/null | wc -l`
if [ $JPG_Count != 0 ]; then 
    echo "Organizing .JPG's..."
    mkdir -p $1/em1/jpg
    mv *.JPG em1/jpg
fi

# Check for .ORFs
ORF_Count=`ls -1 *.ORF 2>/dev/null | wc -l`
if [ $ORF_Count != 0 ]; then 
    echo "Organizing .ORF's..."
    mkdir -p $1/em1/raw
    mv *.ORF em1/raw
fi

# Check for .MOVs
MOV_Count=`ls -1 *.MOV 2>/dev/null | wc -l`
if [ $MOV_Count != 0 ]; then 
    echo "Organizing .MOV's..."
    mkdir -p $1/em1/mov
    mv *.MOV em1/mov
fi

