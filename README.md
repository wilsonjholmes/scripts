# My Scripts

A collection of the scripts I use often.

## Use this after Distro-hopping
```sh
bash -c "$(curl -fsSL https://gitlab.com/wilsonjholmes/scripts/-/raw/main/post-install-scripts/post-install.sh)"
```

## Or this...
```sh
curl -s https://gitlab.com/wilsonjholmes/scripts/-/archive/main/scripts-main.tar.gz\?path\=post-install-scripts -o tmp.tar.gz; tar -xf tmp.tar.gz; cd scripts-main-post-install-scripts/post-install-scripts; ./setup-main.sh; cd ../..; rm -rf tmp.tar.gz scripts-main-post-install-scripts/
```

this is just some test text
