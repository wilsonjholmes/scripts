#!/bin/bash -eu

# Display Help
Help()
{
	echo "Query cht.sh effortlessly! Uses tmux, fzf, curl, and grep."
	echo
	echo "Syntax: cht [-h]"
	echo "options:"
	echo "h     Print this Help."
	echo
}

# required: tmux
if ! command -v tmux &>/dev/null; then
	echo
	echo [!] Command tmux is required. Please install tmux.
	exit 1
else
	echo -e tmux
fi

# required: fzf
if ! command -v fzf &>/dev/null; then
	echo
	echo [!] Command fzf is required. Please install fzf.
	exit 1
else
	echo -e fzf
fi

# required: curl
if ! command -v curl &>/dev/null; then
	echo
	echo [!] Command curl is required. Please install curl.
	exit 1
else
	echo -e curl
fi

# required: grep
if ! command -v grep &>/dev/null; then
	echo
	echo [!] Command grep is required. Please install grep.
	exit 1
else
	echo -e grep
fi

# Get the options
while getopts ":h" option; do
	case $option in
		h) # display Help
			Help
			exit;;
		\?) # Invalid option
			echo "Error: Invalid option"
			exit;;
	esac
done

selected=`cat ~/.tmux-cht-languages ~/.tmux-cht-commands | fzf`
if [[ -z $selected ]]; then
    exit 0
fi

read -p "Enter Query (ENTER if none): " query

# If we are querying about a language
if grep -qs "$selected" ~/.tmux-cht-languages; then
    query=`echo $query | tr ' ' '+'`
    tmux neww bash -c "curl -s cht.sh/$selected/$query | less"
# If we are quering about a command
else
    tmux neww bash -c "curl -s cht.sh/$selected~$query | less"
fi
