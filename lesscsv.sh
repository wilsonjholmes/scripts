#!/bin/bash -e

# TODO: Error message when no file is supplied

# Display Help
Help()
{
	echo "This bash script allows for easy viewing of "
	echo "\`.csv\` files with column and less."
	echo
	echo "Syntax: lesscsv [-h] FILE.csv"
	echo "options:"
	echo "h     Print this Help."
	echo
}

# Get the options
while getopts ":h" option; do
	case $option in
		h) # display Help
			Help
			exit;;
		\?) # Invalid option
			echo "Error: Invalid option"
			exit;;
	esac
done

# Check and see if path is valid
if ! [[ -f "$1" ]]
then
	echo "File path is incorrect, or does not exist."
	exit
fi

# Will view the .csv file supplied with less if no other 
# command like options are given.
column -s, -t < "$1" | less -#2 -N -S
