#!/bin/bash -e

# script to resize images so that they can be used for a website (max resolution:
# 1920x1200).

# Display Help
Help()
{
	echo "This bash script allows for easy resizing of images "
	echo "using ImageMagick."
	echo
	echo "Syntax: resize-images [-h|d|r|q]"
	echo "options:"
	echo "h     Print this Help."
    echo "d     Directory to operate on."
    echo "r     Resolution to resize to."
    echo "q     Quality for the images (1-100)."
	echo
}

# Set default variables
directory="."
resolution="1920x1200"
quality=75

# Get the options
while getopts ":h:d:r:q:" option; do
	case $option in
		h) # display Help
			Help
			exit;;
        d) # directory to operate on
            directory=$OPTARG;;
        r) # resolution to resize to
            resolution=$OPTARG;;
        q) # quality for the images (1-100)
            quality=$OPTARG;;
		\?) # Invalid option
			echo "Error: Invalid option"
			exit;;
	esac
done

cd $directory

# for all .jpg and .png files in the current directory, use convert to resize them to a
# maximum resolution of 1920x1200, and a quality of 75
for f in *.gif; do
    convert "$f" -resize $resolution -quality $quality "$f"
    echo "converting $f"
done
