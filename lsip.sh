#!/bin/bash -eu

Help()
{
	# Display Help
	echo "This bash script uses ip, awk, and arp-scan"
	echo "to display visible devices on a network."
	echo
	echo "Syntax: lsip [-h]"
	echo "options:"
	echo "h     Print this Help."
	echo
}

# required: arp-scan
if ! command -v arp-scan &>/dev/null; then
	echo
	echo [!] Command arp-scan is required. Please install arp-scan.
	exit 1
else
	echo -e arp-scan
fi

# Get the options
while getopts ":h" option; do
	case $option in
		h) # display Help
			Help
			exit;;
		\?) # Invalid option
			echo "Error: Invalid option"
			exit;;
	esac
done

NET_INTERFACE=$(ip addr show | awk '/inet.*brd/{print $NF; exit}')
sudo arp-scan --interface=$NET_INTERFACE --localnet
