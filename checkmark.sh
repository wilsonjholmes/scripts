#!/bin/bash -e

# Display Help
Help()
{
	echo "Easily view \`.md\` files with lowdown and less."
	echo
	echo "Syntax: checkmark [-h] FILE.md"
	echo "options:"
	echo "h     Print this Help."
	echo
}

# required: lowdown
if ! command -v lowdown &>/dev/null; then
	echo
	echo [!] Command lowdown is required. Please install lowdown.
	exit 1
else
	echo -e lowdown
fi

# Get the options
while getopts ":h" option; do
	case $option in
		h) # display Help
			Help
			exit;;
		\?) # Invalid option
			echo "Error: Invalid option"
			exit;;
	esac
done

# Check and see if path is valid
if ! [[ -f "$1" ]]
then
	echo "File path is incorrect, or does not exist."
	exit
fi

lowdown -Tterm $1 | less -R
