#!/bin/bash -eu

Help()
{
	# Display Help
	echo "Will git push for all repos in the current directory."
	echo
	echo "Syntax: pushall [-h] [-v]"
	echo "options:"
	echo "h     Print this Help."
    echo "v     Be more verbose."
	echo
}

Main()
{
    find . -maxdepth 1 -mindepth 1 -type d -exec sh -c '(echo {} && cd {} && git push '"$VERBOSE"')' \;
}

# required: git
if ! command -v git &>/dev/null; then
	echo
	echo [!] Command git is required. Please install git.
	exit 1
else
	echo -e git
fi

# Get the options
while getopts ":hv" option; do
	case $option in
		h) # display Help
			Help
			exit;;
        v) # verbose mode
			VERBOSE="-v"
            Main
			exit;;
        \?) # Invalid option
            echo "Error: Invalid option"
            exit;;
	esac
done

VERBOSE=""
Main
